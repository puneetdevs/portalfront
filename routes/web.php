<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::get('login', 'AuthController@getLogin');

Route::post('login', 'AuthController@postLogin');

Route::get('profile', 'AuthController@getProfile');

Route::get('auth/{token}/{uuid}', 'AuthController@getAuth');

Route::post('change-name', 'AuthController@changeName');

Route::post('logout', 'AuthController@logout');
