@extends('layout.auth')

@section('content')
<div class="row">
    <!-- <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> -->
    <div class="col-lg-12">
        <div class="p-5">
            <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Send Login Link</h1>
            </div>

            @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
            @enderror

            @if (\Session::has('success'))
            <div class="alert alert-success">
                <ul>
                    <li>{!! \Session::get('success') !!}</li>
                </ul>
            </div>
            @endif

            <form method="POST" action="{{url('login')}}" class="user">
                @csrf
                <div class="form-group">
                    <input type="email" name="email" class="form-control form-control-user" aria-describedby="emailHelp"
                        placeholder="Enter Email Address...">
                </div>
                <button type="submit" class="btn btn-primary btn-user btn-block">
                    Login
                </button>
            </form>
            <hr>
            <div class="text-center">
                <a class="small" href="register.html">Create an Account!</a>
            </div>
        </div>
    </div>
</div>
@endsection