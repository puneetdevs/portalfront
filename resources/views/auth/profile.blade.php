@extends('layout.master')

@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Profile</h1>
</div>

<div class="row">
    <div class="col-xl-8 col-lg-7">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Details</h6>
            </div>
            <div class="card-body">
                @error('email')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <form method="POST" action="{{url('change-name')}}" class="user">
                    @csrf
                    <div class="form-group">
                        <input type="email" name="email" value="{{$profile->email}}" class="form-control form-control-user" 
                            aria-describedby="emailHelp" placeholder="Enter Email Address...">
                    </div>

                    <div class="form-group">
                        <input type="text" name="name" value="{{$profile->name}}" class="form-control form-control-user" 
                             placeholder="Enter Name...">
                    </div>
                    <button type="submit" class="btn btn-primary btn-user btn-block">
                        Change Name
                    </button>
                </form>
            </div>
          </div>
    </div>
</div>
@endsection