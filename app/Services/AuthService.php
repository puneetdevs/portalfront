<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use Session;

class AuthService
{
    private $api_url = 'https://portalapi.customerdemourl.com/api/auth';

    private $headers;

    private $http;

    private $token;

    // public function __construct(Http $http)
    // {
    //     $this->http = $http;
    // }

    protected function headers($withAuth = false)
    {
        $this->http = Http::withHeaders([
            'X-Requested-With' => 'XMLHttpRequest'
        ]);

        if($withAuth){
            $this->http->withToken(session('token'));
        }

        return $this->http;
    }

    public function login($data)
    {
        $login = $this->headers()->post($this->api_url.'/login', [
            'email' => $data['email']
        ]);

        return $login;
    }

    public function profile()
    {
        $profile = $this->headers(true)->post($this->api_url.'/me', [
            'uuid' => session('uuid')
        ]);

        return $profile;
    } 

    public function changeName($data)
    {
        $changeName = $this->headers(true)->post($this->api_url.'/change-name', [
            'uuid' => session('uuid'),
            'name' => $data['name'],
            'email'=> $data['email']
        ]);

        return $changeName;
    }

    public function logout()
    {
        $logout = $this->headers(true)->post($this->api_url.'/logout', []);

        return $logout;
    }
    
}
