<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\AuthService;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\ProfileRequest;

class AuthController extends Controller
{
    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function getLogin()
    {
        return view('auth.login');
    }

    public function postLogin(LoginRequest $request)
    {
        $login = $this->authService->login($request);

        if($login->status() == '200')
        {
            return redirect()->back()->withSuccess('Email sent successfully');
        }elseif($login->status() == '404'){
            return redirect()->back()->withErrors('Email Not found.');
        }
    }

    public function getAuth($token, $uuid)
    {
        session([
            'token' => $token,
            'uuid'  => $uuid
        ]);

        return redirect('profile');
    }

    public function getProfile()
    {
        $profile = $this->authService->profile();

        if($profile->status() == '401')
        {
            session()->flush();

            return redirect('login');
        }

        return view('auth.profile')->with('profile', json_decode($profile->body()));
    }

    public function changeName(ProfileRequest $request)
    {
        $changeName = $this->authService->changeName($request);

        return redirect('profile');
    }

    public function logout(Request $request)
    {
        $logout = $this->authService->logout();

        if($logout->successful()){
            return redirect('login');
        }
    }
}
